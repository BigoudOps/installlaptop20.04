#!/bin/bash
rouge='\e[1;31m'
neutre='\e[0;m'
vert='\e[4;32m'
bleu='\e[1;34m'
jaune='\e[1;33m'
if [ "$UID" -eq "0" ]; then
    echo -e "${rouge}lance le sans sudo, le mot de passe sera demandé dans le terminal lors de la 1ère action nécessitant le droit administrateur.${vert}"
    exit
fi
sudo apt install -y apt-transport-https gnupg-agent software-properties-common
if [ ! -f "/usr/bin/git" ]; then
    echo -e "${vert}installation de git${neutre}"
    sudo apt install -y git
else
    echo -e "${rouge}Git est déjà installer${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi

if [ ! -f "/usr/bin/wget" ]; then
    echo -e "${vert}installation de wget pour l'installation des paquets deb${neutre}"
    sudo apt install wget
else
    echo -e "${rouge}wget est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/etc/ca-certificates" ]; then
    sudo apt install -y ca-certificates
else
    echo -e "${rouge}ca-certificateq est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/curl" ]; then
    echo -e "${bleu}installation de curl ${neutre}"
    sudo apt install -y curl
else
    echo -e "${rouge}curl est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/gnome-tweaks" ]; then
    echo -e "${jaune}installer gnome-tweaks?${neutre}[O/n]"
    read -r -r gnome_tweaks
    case $gnome_tweaks in
    N | n)
        echo "Gnome-tweaks ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de gnome-tweaks${neutre}"
        sudo apt install -y gnome-tweaks
        ;;
    esac
else
    echo -e "${rouge}gnome-tweaks est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/$HOME/.bashrc.omb-backup" ]; then
    echo -e "${vert}installer oh my bash? ${neutre} [O/n]"
    read -r ohmy
    case $ohmy in
    N | n)
        echo "oh my bash ne sera pas installé."
        echo "Le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation d'oh my bash {$neutre}"
        bash -c "$(wget https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh -O -)"
        ;;
    esac
else
    echo -e "${rouge}oh my bash est déjà installé {$neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi

echo -e "${bleu}installation de VSCodium ${neutre}"
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
sudo apt update && sudo apt install -y codium
if [ ! -f "/usr/bin/docker" ]; then
    echo -e "${vert}voulez-vous installer docker?${neutre}[O/n]"
    read -r docker
    case $docker in
    N | n)
        echo "Docker ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation de docker ${neutre}"
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose
        sudo usermod -aG docker "$USER"
        ;;
    esac
else
    echo -e "${rouge}docker est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/ansible" ]; then
    echo -e "${bleu}voulez-vous installer ansible?${neutre}[O/n]"
    read -r ansible
    case $ansible in
    N | n)
        echo "Ansible ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}installation de ansible${neutre}"
        sudo apt-add-repository --yes --update ppa:ansible/ansible
        sudo apt install -y ansible
        ;;
    esac
else
    echo -e "${rouge}ansible est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi
if [ ! -f "/bin/gdebi" ]; then
    echo -e "${jaune}voulez-vous installer l'utilitaire gdebi permettant l'installation de paquet deb?${neutre}[O/n]"
    read -r gdebi
    case $gdebi in
    N | n)
        echo "L'utilitaire gdebi ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de l'utilitaire gdebi permettant l'installation de paquet deb${neutre}"
        sudo apt install -y gdebi-core
        ;;
    esac
else
    echo -e "${rouge}Gdebi est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/chrome-gnome-shell" ]; then
    echo -e "${vert}voulez-vous installer le paquet chrome-gnome-shell?${neutre}[O/n]"
    read -r gnome_shell
    case $gnome_shell in
    N | n)
        echo "Le paquet chrome-gnome-shell ne sera pas installé."
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation du paquet chrome-gnome-shell${neutre}"
        sudo apt install -y chrome-gnome-shell
        ;;
    esac
else
    echo -e " ${vert}chrome-gnome-shell est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/sbin/synaptic" ]; then
    echo -e "${bleu}installer synaptic? ${neutre}[O/n]"
    read -r synaptic
    case $synaptic in
    N | n)
        echo "synaptic ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu} installation de synaptic manager${neutre}"
        sudo apt install -y synaptic
        ;;
    esac
else
    echo -e "${rouge}synaptic manager est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/sushi" ]; then
    echo -e "${jaune}installer gnome-sushi ?${neutre}[O/n]"
    read -r gnome_sushi
    case $gnome_sushi in
    N | n)
        echo "gnome-sushi ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de gnome-sushi ${neutre}"
        sudo apt install -y gnome-sushi
        ;;
    esac
else
    echo -e "${rouge}gnome-sushi est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/keepassxc" ]; then
    echo -e "${jaune}keepassxc n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r keepassxc
    case $keepassxc in
    N | n)
        echo "keepassxc ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation du ppa keepassxc ${neutre}"
        sudo add-apt-repository --yes --update ppa:phoerious/keepassxc
        sudo apt install -y keepassxc
        ;;
    esac
else
    echo -e "${rouge}keepassxc est déjà installé${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/virtualbox" ]; then
    echo -e "${jaune}Virtualbox n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r virtual
    case $virtual in
    N | n)
        echo "Virtualbox ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation de Virtualbox ${neutre}"
        sudo apt install -y virtualbox
        sudo apt install -y virtualbox-ext-pack
        ;;
    esac
else
    echo -e "${rouge}Virtualbox est déjà installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/virt-manager" ]; then
    echo -e "${vert}Qemu n'est pas installé voulez-vous l'installer? ${neutre}[O/n]"
    read -r qemu
    case $qemu in
    N | n)
        echo "Qemu ne sera pas installé"
        echo "le script continue la verification et l'installation des programmes..."
        ;;
    O | o | *)
        echo -e "${vert}Installation de Qemu${neutre}"
        sudo apt install -y qemu qemu-kvm libvirt-daemon libvirt-clients bridge-utils virt-manager
        sudo systemctl enable --now libvirtd # enable on boot
        sudo apt install -y uvtool
        ;;
    esac
else
    echo -e "${rouge}Qemu est déjà installé ${neutre}"
    echo "le script continue la verification et l'installation des programmes"
fi

if [ ! -f "/usr/bin/shellcheck" ]; then
    echo -e "${vert}ShellCheck n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r shell
    case $shell in
    N | n)
        echo "ShellCheck ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}installation de ShellCheck ${neutre}"
        sudo apt install -y shellcheck
        ;;
    esac
else
    echo -e "${rouge}ShellCheck est déja installé ${neutre}"
    echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -d "$HOME/Images/Wallpapers/wall" ]; then
    echo -e "${bleu}Installer le wallpaper dynamique?${neutre}[0/n]"
    read -r wall
    case $wall in
    N | n)
        echo "Le wallpaper dynamique ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}Installation du wallpaper dynamique${neutre}"
        mkdir -p "$HOME"/Images/Wallpapers/wall
        cp -f src/BigApple.jpg "$HOME"/Images/Wallpapers/wall/
        cp -f src/gotham_sirens_by_tsubs_ddr96jv.jpg "$HOME"/Images/Wallpapers/wall/
        cp -f src/lagarennecolombes--colombes.jpg "$HOME"/Images/Wallpapers/wall/
        cp -f src/Yennefer_of_Vengerberg_by_Vasiliell.jpg "$HOME"/Images/Wallpapers/wall/
        cp -f src/wall.xml "$HOME"/Images/Wallpapers/wall/
        sed -i "s/changeme/$USER/g" "/home/$USER/Images/Wallpapers/wall/wall.xml"
        ;;
    esac
else
    echo -e "${bleu}Installer le wallpaper dynamique?${neutre}[0/n]"
    read -r wally
    case $wally in
    N | n)
        echo "Le wallpaper dynamique ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}Installation du wallpaper dynamique${neutre}"
        mkdir -p "$HOME"/Images/Wallpapers/wall
        cp src/BigApple.jpg "$HOME"/Images/Wallpapers/wall/
        cp src/gotham_sirens_by_tsubs_ddr96jv.jpg "$HOME"/Images/Wallpapers/wall/
        cp src/lagarennecolombes--colombes.jpg "$HOME"/Images/Wallpapers/wall/
        cp src/Yennefer_of_Vengerberg_by_Vasiliell.jpg "$HOME"/Images/Wallpapers/wall/
        cp src/wall.xml "$HOME"/Images/Wallpapers/wall/
        sed -i "s/changeme/$USER/g" "/home/$USER/Images/Wallpapers/wall/wall.xml"
        ;;
    esac

fi
if [ ! -d "/usr/share/themes/Neon-Originals-Shell-40-Prime-3" ]; then
    echo -e "${jaune}Neon-Original-shell n'est pas installé voulez-vous l'installer?${neutre}[O/n]"
    read -r neon
    case $neon in
    N | n)
        echo "Neon-Originals-shell ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${jaune}installation du theme Neon-Oiginals-Shell-40-Prime-3 ${neutre}"
        sudo unzip -q src/Neon-Originals-Shell-40-Prime-3.zip -d /usr/share/themes/
        ;;
    esac

else
    echo -e "${rouge}Nothing to do here ${neutre}"
fi
if [ ! -d "/usr/share/icons/Fancy-Dark-Icons" ]; then
    echo -e "${jaune}Installer Fancy-Dark-Icons?${neutre}[O/n]"
    read -r fancy
    case $fancy in
    N | n)
        echo "Fancy-Dark-Icons ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${bleu}Installation de Fancy-Dark-Icons${neutre}"
        sudo tar -zxvf src/Fancy-Dark-Icons.tar.gz -C /usr/share/icons/
        echo -e "${vert}Done${neutre}"
        ;;
    esac
else
    echo -e "${vert}Fancy-Dark-Icons est déjà installer ${neutre}"
fi
if [ ! -d "/usr/share/icons/Infinity-Dark-Icons" ]; then
    echo -e "${bleu}Installer Infinity-Dark-Icons?${neutre}[O/n]"
    read -r infinity
    case $infinity in
    N | n)
        echo "Infinity-Dark-Icons ne sera pas installé"
        echo "le script continue la vérification et l'installation des programmes"
        ;;
    O | o | *)
        echo -e "${vert}Installation de Infinity-Dark-Icons${neutre}"
        sudo tar -zxvf src/Infinity-Dark-Icons.tar.gz -C /usr/share/icons/
        echo -e "${jaune}C'est fait${neutre}"
        ;;
    esac
else
    echo -e "${vert}Infinity-Dark-Icons est déjà installer${neutre}"
fi
echo -e "${rouge}Copier manuellement codium-url-handler.desktop ${neutre}"
