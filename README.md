# This is my script post install for my old laptop be happy & use olds stuff since 2011 😜 

[![Join the Discord channel](https://img.shields.io/static/v1.svg?label=%20Rejoignez-moi%20sur%20Discordl&message=%F0%9F%8E%86&color=7289DA&logo=discord&logoColor=white&labelColor=2C2F33)](https://discord.gg/bfB6Ve6) 

![visitors](https://visitor-badge.glitch.me/badge?page_id=BigoudOps.readme)

[![Reddit profile](https://img.shields.io/reddit/subreddit-subscribers/apdm?style=social)](https://www.reddit.com/r/apdm) 

## Config in Firefox for url-handler git
1) first go `about:config` in firefox
2) enter this network.protocol-handler.external.git then add in String  `/usr/share/codium/codium --no-sandbox --open-url %U `
3) modify your .desktop in `/usr/share/applications/` then type `sudo update-desktop-database`
4) It's done 👌

## Sources for files in src :

- Fancy-Dark-icons.tar.gz 🔜 https://www.gnome-look.org/p/1598639
- Infinity-Dark-Icons.tar.gz 🔜 https://www.gnome-look.org/p/1436570
- Neon-Originals-Shell-40-Prime-3.zip 🔜 https://www.gnome-look.org/p/1449493
- lagarennecolombes--colombes.jpg create by 🔜  http://www.studioartcontrast.com 
- gotham_sirens_by_tsubs_ddr96jv 🔜 [deviantart](https://www.deviantart.com/tsubs/art/Gotham-Sirens-831838315)
- Yennefer_of_Vengerberg_by_Vasiliell.jpg 🔜 [deviantart](https://www.deviantart.com/vasiliell/art/Yennefer-of-Vengerberg-831916463)
- BigApple.jpg 🔜 [deviantArt](https://www.deviantart.com/orioto/art/Big-Apple-3AM-769791502)
  